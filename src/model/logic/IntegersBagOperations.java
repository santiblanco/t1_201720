package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {



	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}


	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
		int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}

		}
		return max;
	}

	public int getMin(IntegersBag bag){
		int min = Integer.MAX_VALUE;
		int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( min > value){
					min = value;
				}
			}

		}
		return min;
	}

	public boolean IsNumberThere(IntegersBag bag, int num){
		int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( num == value){
					return true;
				}
			}

		}
		return false;
	}
	public boolean addNumber (IntegersBag bag, int num){
		if(bag != null){
			if(IsNumberThere(bag, num) == false){
				bag.addDatum(num);
				return true;
			}

		}
		return false;
	}
}
